<?php
/**
 * Created by PhpStorm.

 */

error_reporting(E_ALL);

require_once('app/Mage.php'); //Path to Magento
umask(0);
Mage::app();



class Highdigital_Modmaninstall
{

    private  $token = 'de7d19ba92c16a967e97f71b5dafbf41604e2955';
    private  $apiUrlGitHub = 'https://api.github.com/';

    public function run()
    {
        ini_set('display_errors',1);
        echo 'Aktuelles System: ' . php_uname('n')."<br/>";


        if($this->_initPublicKey() == true){

            $this->_initConfig();

            if(file_exists(Mage::getBaseDir('base').'/modman') == false) {
                exec('wget -q --no-check-certificate - https://raw.github.com/colinmollenhour/modman/master/modman');
                exec('chmod +x '.Mage::getBaseDir('base').'/modman' ,$log,$result);
            }
            if($this->modmanInit() == true){
                $this->_initKnownHosts();

            };
            if($_GET['modmanmodul'] == true ){


                echo('start installing modmanmodul');
                $this->_initModmanModul();

            }
        }
    }

    public function update()
    {
        ini_set('display_errors',1);
        echo 'Aktuelles System: ' . php_uname('n')."<br/>";

        exec(Mage::getBaseDir('base').'/modman update highdigital-modman  --force 2>&1', $result);

        $resultString = implode('<br/>',$result);

        echo $resultString."\n";
        if(strpos($resultString,'has already been checked out') || strpos($resultString,'Error trying to update')){
            foreach($result as $line){

                echo $line.'<br/>';
            }
            echo Mage::helper('modman')->__($resultString);

        } else {

            echo('Highdigital Modman wurde erfolgreich aktualisiert.');

        }

        return true;



    }

    public function convert()
    {
        ini_set('display_errors',1);
        echo 'Aktuelles System: ' . php_uname('n')."<br/>";


        $currentUser = $this->getWebUser();
        echo "webuser: $currentUser <br/>";
        exec('echo ~'.$currentUser,$userHomeDirectory);
        $userHomeDir = $userHomeDirectory[0].DS;
        $filePath = $userHomeDir.'.ssh/id_rsa.pub';

        if(!$this->getGithubKeyExists()){
            $this->setGithubPublicKey();
        }

        exec(Mage::getBaseDir('base').'/modman remove highdigital-modman', $log, $result);
        exec(Mage::getBaseDir('base').'/modman clone git@github.com:highdigital/highdigital-modman.git  2>&1', $log, $result);
        if($result){
            echo('Das Remote Repository konnte nicht ge‰ndert werden');
        } else {
            echo('Das Remote Repository von Highdigital Modman liegt nun bei Github');
        }

        return true;

    }
    protected function _initPublicKey(){
        $pubkeyExists = false;
        $currentUser = $this->getWebUser();
        echo "webuser: $currentUser <br/>";
        exec('echo ~'.$currentUser,$userHomeDirectory);
        $userHomeDir = $userHomeDirectory[0].DS;

        if(is_link($userHomeDirectory[0])){
            $userHomeDir = dirname($userHomeDir).DS.readlink($userHomeDirectory[0]).DS;
        }
        echo "webuser Homedir: $userHomeDir <br/>";
        if(!is_dir($userHomeDir.'.ssh')){
            echo "Verzeichnis $userHomeDir.ssh erstellt<br/>";
            mkdir($userHomeDir.'.ssh');
        }
        if(file_exists($userHomeDir.'.ssh/id_rsa.pub')== false){
            if($_GET['keygen']== true){
                $config = array(
                    "digest_alg" => "sha512",
                    "private_key_bits" => 4096,
                    "private_key_type" => OPENSSL_KEYTYPE_RSA,
                );

                // Create the private and public key
                $res = openssl_pkey_new($config);

                // Extract the private key from $res to $privKey
                openssl_pkey_export($res, $privKey);

                // Extract the public key from $res to $pubKey
                //$pubKey = openssl_pkey_get_details($res);

                $pubKey = $this->sshEncodePublicKey($res); //Public Key

                file_put_contents($userHomeDir.'.ssh/id_rsa', $privKey); //save private key into file
                file_put_contents($userHomeDir.'.ssh/id_rsa.pub', $pubKey); //save public key into file

                exec('chmod 0700 '.$userHomeDir.'.ssh',$log,$result);
                exec('chmod 0600 '.$userHomeDir.'.ssh/id_rsa',$log,$result);

                echo 'Pubkey wurde angelegt in '.$userHomeDir.'<br/>';
            }
            else {
                exec('ssh-keygen -t rsa -P "" -b 4096 -f '.$userHomeDir.'.ssh/id_rsa',$log,$result);
                exec('chown -R '.$currentUser.':'. $currentUser .' '.$userHomeDir.'.ssh',$log,$result);
                exec('chmod 0700 .ssh',$log,$result);
                exec('chmod 0600 .ssh/id_rsa',$log,$result);
                if(file_exists($userHomeDir.'.ssh/id_rsa.pub') == false){
                    echo 'Pubkey wurde nicht angelegt <br/>';
                } else {
                    echo 'Pubkey wurde angelegt in '.$userHomeDir.'<br/>';
                    $pubkeyExists = true;
                }
            }
        } else {
            echo 'Pubkey existiert <br/>';
            $pubkeyExists = true;
        }

        $filePath = $userHomeDir.'.ssh/id_rsa.pub';

        if(!$this->getGithubKeyExists(file_get_contents($filePath))){
            $this->setGithubPublicKey(file_get_contents($filePath));
        }
        return $pubkeyExists;
    }

    private function sshEncodePublicKey($privKey) {
        $keyInfo = openssl_pkey_get_details($privKey);
        $buffer  = pack("N", 7) . "ssh-rsa" .
            $this->sshEncodeBuffer($keyInfo['rsa']['e']) .
            $this->sshEncodeBuffer($keyInfo['rsa']['n']);
        return "ssh-rsa " . base64_encode($buffer);
    }

    private function sshEncodeBuffer($buffer) {
        $len = strlen($buffer);
        if (ord($buffer[0]) & 0x80) {
            $len++;
            $buffer = "\x00" . $buffer;
        }
        return pack("Na*", $len, $buffer);
    }


    private function getGithubKeyExists($localKey = null){

        $token = $this->token;
        $_apiBaseUrl = $this->apiUrlGitHub;
        $_apiFunctionPath = 'user/keys';
        $exists = false;

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $_apiBaseUrl.$_apiFunctionPath);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array('Authorization: token '.$token));
        curl_setopt($curl, CURLOPT_USERAGENT,'Highdigital-Admin-App');
        $response = curl_exec($curl);

        // close curl resource to free up system resources
        curl_close($curl);
        $keys = json_decode($response, true);
        foreach($keys as $key){

            if($localKey == $key['key']){

                $exists = true;

            }

        }
        return $exists;

    }
    private function setGithubPublicKey($key){

        $token = $this->token;
        $_apiBaseUrl = $this->apiUrlGitHub;
        $_apiFunctionPath = 'user/keys';

        $keydata = array(
            "title"=> gethostname().'-'.$_SERVER['SERVER_ADDR'],
            "key"=> $key
        );
        $keydata = json_encode($keydata);
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $_apiBaseUrl.$_apiFunctionPath);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array('Authorization: token '.$token));
        curl_setopt($curl, CURLOPT_USERAGENT,'Highdigital-Admin-App');
        curl_setopt($curl,CURLOPT_POST, count($keydata));
        curl_setopt($curl,CURLOPT_POSTFIELDS, $keydata);
        $response = curl_exec($curl);

        // close curl resource to free up system resources
        curl_close($curl);

        $result = json_decode($response, true);

        if($result['verified']){


            echo 'Pubkey wurde bei GitHub angelegt<br/>';

        } else if(array_key_exists('errors',$result) && $result['errors'][0]['message'] == 'key is already in use'){


            echo 'Pubkey existiert schon bei GitHub<br/>';

        } else {
            echo 'Pubkey konnte nicht bei GitHub angelegt werden<br/>';

        }

    }
    private function _initConfig(){

        $currentUser = $this->getWebUser();
        exec('echo ~'.$currentUser,$userHomeDir);
        $userHomeDir = $userHomeDir[0].DS;

        if(!is_dir($userHomeDir.'.ssh')){

            mkdir($userHomeDir.'.ssh');
        }

        if(file_exists($userHomeDir.'.ssh/config') == false){

            exec('touch '.$userHomeDir.'.ssh/config');
            exec('echo "ServerAliveInterval 60

Host *
ForwardAgent yes
StrictHostKeyChecking no" > '.$userHomeDir.'.ssh/config');

            exec('chmod 0700 '.$userHomeDir.'.ssh/config',$log,$result);
        }
    }
    protected function modmanInit(){
        if(!is_dir(Mage::getBaseDir('base').'/.modman')){

            $currentUser = $this->getWebUser();
            exec('chown '.$currentUser.' '.Mage::getBaseDir('base').'/modman');
            exec('chmod +x '.Mage::getBaseDir('base').'/modman' ,$log,$result);
            exec(Mage::getBaseDir('base').'/modman init' , $result, $return);
            $result = implode('<br/>',$result);
            echo($result);
            return true;
        } else {

            echo 'modman Verzeichnis existiert bereits<br/>';
        }

    }
    protected function _initKnownHosts(){
        $gitUrl = 'projekt.highdigital.de';
        $currentUser = $this->getWebUser();
        exec('echo ~'.$currentUser,$userHomeDir);
        exec('ssh-keyscan -H -t ecdsa '.$gitUrl.' >> '.$userHomeDir[0].'/.ssh/known_hosts',$report,$return);
        return $report;
    }
    protected function _initModmanModul(){


        exec(Mage::getBaseDir('base').'/modman clone git@github.com:highdigital/highdigital-modman.git --force --copy  2>&1', $result);

        $resultString = implode('<br/>',$result);

        echo $resultString."\n";
        if(strpos($resultString,'has already been checked out') || strpos($resultString,'Error trying to clone')){
            foreach($result as $line){

                echo $line.'<br/>';
            }
            echo Mage::helper('modman')->__($resultString);

        } else {

            echo('modman modul installiert.');

        }

        return true;

    }
    protected function _getGitHEADId($modmanName = false){
        if ($modmanName == false){

            $modmanName = $this->_getModmanName();

        }

        try{
            chdir(Mage::getBaseDir('base').'/.modman/'. $modmanName);
            $git_version = shell_exec('git rev-parse HEAD');

            $this->setData('git_revision',$git_version);
            return $git_version;

        }
        catch(Exception $e){
            Mage::logException($e);
        }
    }
    protected function getWebUser(){
        if(function_exists(posix_getpwuid)){
            $processUser = posix_getpwuid(posix_geteuid());
            $currentUser = $processUser['name'];
        } else if($_GET['webuser']){

            $currentUser = $_GET['webuser'];
        } else {

            echo('no webuser defiend');

        }

        return $currentUser;

    }
}
if($_GET['install']== true){
    $modmanInstall = new Highdigital_Modmaninstall();
    $modmanInstall->run();
}

if($_GET['update']==true){

    $modmanInstall = new Highdigital_Modmaninstall();
    $modmanInstall->update();

}

if($_GET['convert']==true){

    $modmanInstall = new Highdigital_Modmaninstall();
    $modmanInstall->convert();

}