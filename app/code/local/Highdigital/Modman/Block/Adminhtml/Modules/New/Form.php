<?php

class Highdigital_Modman_Block_Adminhtml_Modules_New_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{

		$data = array();
		
		//print_r($data);
		$form = new Varien_Data_Form(array(
				'id' => 'edit_form',
				'action' => $this->getUrl('*/*/savekeys'),
				'method' => 'post',
				'enctype' => 'multipart/form-data',
		));

		$form->setUseContainer(true);

		$this->setForm($form);

		$fieldset = $form->addFieldset('modman_form', array(
				'legend' =>Mage::helper('modman')->__('Private and public keys')
		));

		$fieldset->addField('privatekey', 'textarea', array(
				'label'     => Mage::helper('modman')->__('Private key'),
				'class'     => 'required-entry',
				'required'  => true,
				'name'      => 'privatekey',
		));

		$fieldset->addField('publickey', 'textarea', array(
				'label'     => Mage::helper('modman')->__('Public key'),
				'class'     => 'required-entry',
				'required'  => true,
				'name'      => 'publickey',
		));

		$form->setValues($data);

		return parent::_prepareForm();
	}
}