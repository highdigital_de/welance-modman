<?php
class Highdigital_Modman_Block_Adminhtml_Modules_Edit extends Mage_Adminhtml_Block_Abstract{
	
	protected function _construct(){
		
	}
	
	protected function _getGitStatus(){
		
		
		exec('git --version', $git_version, $return);
		
		return $git_version;
	}
	
	protected function _getModmanStatus(){
		
		
		exec(Mage::getBaseDir('base').'/modman --version', $modman_version, $return);
		
		return $modman_version;
	}
	protected function getModmanModul(){
		
		return Mage::getModel('modman/modules')->load($this->getRequest()->getParam('id'));
		
	}

	protected function addModul($modman){
		
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
	 
	    $table = $resource->getTableName('modman/modules');
	     
	    $query = "INSERT {$table} (
				`resource_id` ,
				`git_clone_url` ,
				`sort_order` ,
				`modman_name` ,
				`status`
				)
				VALUES (
				NULL ,  '',  '0',  '{$modman}',  'Activ'
				)";
	     
	    $writeConnection->query($query);
		
	}
	

	protected function addModules($list){
		
		foreach($list as $modul){
			
			$this->addModul($modul);
			
		}
		
	}

	protected function deactivateModul($modman){
		
	    $resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
	 
	    $table = $resource->getTableName('modman/modules');
	     
	    $query = "UPDATE {$table} SET status = 'Disabled' WHERE modman_name = '{$modman}'";
	    $writeConnection->query($query);
		
	}
	

	protected function deactivateModules($list){
		
		foreach($list as $modul){
			
			$this->deactivateModul($modul);
			
		}
		
	}
	
}