<?php

class Highdigital_Modman_Block_Adminhtml_Modules_New extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		parent::__construct();

		$this->_objectId = 'resource_id';
		$this->_blockGroup = 'modman';
		$this->_controller = 'adminhtml_modules';
		$this->_mode = 'new';

	}

	public function getHeaderText()
	{
	
		return Mage::helper('modman')->__('Create public and private key');
	}
	
	protected function _prepareLayout()
	{
		if ($this->_blockGroup && $this->_controller && $this->_mode) {
			$this->setChild('form', $this->getLayout()->createBlock($this->_blockGroup . '/' . $this->_controller . '_' . $this->_mode . '_form'));
		}
		return parent::_prepareLayout();
	}

}