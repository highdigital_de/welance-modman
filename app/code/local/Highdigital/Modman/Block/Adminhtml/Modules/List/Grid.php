<?php

class Highdigital_Modman_Block_Adminhtml_Modules_List_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('modman_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('modman/modules')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
 
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('modman')->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'id',
        ));
 
        $this->addColumn('name', array(
            'header'    => Mage::helper('modman')->__('Name'),
            'align'     =>'left',
            'index'     => 'name',
        ));
 
        // $this->addColumn('description', array(
            // 'header'    => Mage::helper('awesome')->__('Description'),
            // 'align'     =>'left',
            // 'index'     => 'description',
        // ));
//  
        // $this->addColumn('other', array(
            // 'header'    => Mage::helper('awesome')->__('Other'),
            // 'align'     => 'left',
            // 'index'     => 'other',
        // ));
 
        return parent::_prepareColumns();
    }
 
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getEntityId()));
    }
}