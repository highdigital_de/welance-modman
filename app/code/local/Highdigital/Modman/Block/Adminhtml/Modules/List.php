<?php
class Highdigital_Modman_Block_Adminhtml_Modules_List extends Mage_Adminhtml_Block_Abstract{
	
	protected $_addButtonLabel = 'Add New Example';
 
    public function __construct()
    {
        parent::__construct();
        $this->_controller = 'adminhtml_modules_list';
        $this->_blockGroup = 'modman';
        $this->_headerText = Mage::helper('modman')->__('Examples');
    }
	protected function _prepareLayout()
   {
       $this->setChild( 'grid',
           $this->getLayout()->createBlock( $this->_blockGroup.'/' . $this->_controller . '_grid',
           $this->_controller . '.grid')->setSaveParametersInSession(true) );
       return parent::_prepareLayout();
   }
	
	protected function _getGitStatus(){
		
		
		$git_version = shell_exec('git --version');
		
		return $git_version;
	}
	
	protected function _getModmanStatus(){
		
		exec(Mage::getBaseDir('base').'/modman status', $modman_modmanstatus, $return);
		if(in_array('Module Manager directory not found.',$modman_modmanstatus)){
		
            $modman_version = false;
			
		} else {
			
			exec('./modman --version', $modman_version, $return);
			
		}
		return $modman_version;
	}
	
	
	protected function getModmanList(){
		
		$result = array();

		exec(Mage::getBaseDir('base').'/modman list', $modman_modullist, $return);
		
		if(Mage::getModel('modman/modules')->getCollection()->count()==0){
			
			foreach($modman_modullist as $modman){

				$this->addModul($modman);
				
			}
			
		} else {
			
			$modman_installed = array();
			foreach(Mage::getModel('modman/modules')->getCollection() as $modul){
				$modman_installed[] = $modul->getModmanName();
			}
			
			// $this->deactivateModules(array_diff($modman_installed,$modman_modullist));
			// $this->addModules(array_diff( $modman_modullist, $modman_installed));
		}
			
		$db_modules = Mage::getModel('modman/modules')->getCollection()
				->setOrder('sort_order', 'ASC');
		
		//print_r($db_modules);die();
		
		return $db_modules;
		
	}

	protected function addModul($modman){
		
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
	 
	    $table = $resource->getTableName('modman/modules');
	     
	    $query = "INSERT {$table} (
				`resource_id` ,
				`git_clone_url` ,
				`sort_order` ,
				`modman_name` ,
				`status`
				)
				VALUES (
				NULL ,  '',  '0',  '{$modman}',  'activ'
				)";
	     
	    $writeConnection->query($query);
		
	}
	protected function addModules($list){
		
		foreach($list as $modul){
			
			$this->addModul($modul);
			
		}
		
	}

	protected function deactivateModul($modman){
		
	    $resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
	 
	    $table = $resource->getTableName('modman/modules');
	     
	    $query = "UPDATE {$table} SET status = 'source_deleted' WHERE modman_name = '{$modman}'";
	    $writeConnection->query($query);
		
	}
	

	protected function deactivateModules($list){
		
		foreach($list as $modul){
			
			$this->deactivateModul($modul);
			
		}
		
	}
	
}