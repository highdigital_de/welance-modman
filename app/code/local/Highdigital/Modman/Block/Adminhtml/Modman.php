<?php
 
class Highdigital_Modman_Block_Adminhtml_Modman extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    protected $_addButtonLabel = 'Add New Modman Modul';
 
    public function __construct()
    {
        parent::__construct();
        $this->_controller = 'adminhtml_modman';
        $this->_blockGroup = 'modman';
        $this->_headerText = Mage::helper('modman')->__('Modman Modules');
    }
	
	protected function _prepareLayout()
   {
       $this->setChild( 'grid',
           $this->getLayout()->createBlock( $this->_blockGroup.'/' . $this->_controller . '_grid',
           $this->_controller . '.grid')->setSaveParametersInSession(true) );
       return parent::_prepareLayout();
   }
}