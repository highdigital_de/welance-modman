<?php

class Highdigital_Modman_Block_Adminhtml_Modman_New_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{

		if (Mage::getSingleton('adminhtml/session')->getModmanData())
		{
			$data = Mage::getSingleton('adminhtml/session')->getModmanData();
			Mage::getSingleton('adminhtml/session')->getModmanData(null);
		}
		elseif (Mage::registry('modman_data'))
		{
			$data = Mage::registry('modman_data')->getData();
		}
		else
		{
			$data = array();
		}
		
		//print_r($data);
		$form = new Varien_Data_Form(array(
				'id' => 'edit_form',
				'action' => $this->getUrl('*/*/save', array('resource_id' => $this->getRequest()->getParam('resource_id'))),
				'method' => 'post',
				'enctype' => 'multipart/form-data',
		));

		$form->setUseContainer(true);

		$this->setForm($form);

		$fieldset = $form->addFieldset('modman_form', array(
				'legend' =>Mage::helper('modman')->__('Modman Information')
		));

		$fieldset->addField('git_clone_url', 'text', array(
				'label'     => Mage::helper('modman')->__('Git url'),
				'class'     => 'required-entry',
				'required'  => true,
				'name'      => 'git_clone_url',
				'note'     => Mage::helper('modman')->__('The URL of the Repo.'),
		));

		$fieldset->addField('git_branch', 'text', array(
				'label'     => Mage::helper('modman')->__('Branch'),
				'required'  => false,
				'name'      => 'git_branch',
		));

        $fieldset->addField('auto_update', 'select', array(
            'label'     => Mage::helper('modman')->__('Automatic Updates'),
            'required'  => false,
            'value'     => true,
            'values'    => Mage::getModel('adminhtml/system_config_source_yesno')->toArray(),
            'name'      => 'auto_update',
        ));

		$form->setValues($data);

		return parent::_prepareForm();
	}
}