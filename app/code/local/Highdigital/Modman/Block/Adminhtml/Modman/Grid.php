<?php
 
class Highdigital_Modman_Block_Adminhtml_Modman_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('modman_grid');
        $this->setDefaultSort('modman_name');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('modman/modules')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
 
    protected function _prepareColumns()
    {
        /*$this->addColumn('resource_id', array(
            'header'    => Mage::helper('modman')->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'resource_id',
        ));*/
 
        $this->addColumn('modman_name', array(
            'header'    => Mage::helper('modman')->__('Modman name'),
            'align'     =>'left',
            'index'     => 'modman_name',
        ));
 
        $this->addColumn('git_clone_url', array(
            'header'    => Mage::helper('modman')->__('Clone url'),
            'align'     =>'left',
            'index'     => 'git_clone_url',
        ));
 
        $this->addColumn('git_branch', array(
            'header'    => Mage::helper('modman')->__('Branch'),
            'align'     =>'left',
            'index'     => 'git_branch',
        ));


        $this->addColumn('status', array(
        		'header'    => Mage::helper('modman')->__('Status'),
        		'align'     =>'left',
        		'index'     => 'status',
        ));
		


        $this->addColumn('revision', array(
        		'header'    => Mage::helper('modman')->__('Revision'),
        		'align'     =>'left',
        		'index'     => 'git_revision',
        ));
        $this->addColumn('last_update', array(
            'header'    => Mage::helper('modman')->__('Last update'),
            'align'     =>'left',
            'index'     => 'last_update',
        ));
        $this->addColumn('status', array(
                'header'    => Mage::helper('modman')->__('Status'),
            'align'     =>'left',
            'index'     => 'status',
            'renderer'  => 'Highdigital_Modman_Block_Adminhtml_Modman_Renderer_Status',
        ));
//  
        // $this->addColumn('other', array(
            // 'header'    => Mage::helper('modman')->__('Other'),
            // 'align'     => 'left',
            // 'index'     => 'other',
        // ));
 
        return parent::_prepareColumns();
    }
 
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('resource_id' => $row->getResourceId()));
    }
    
    protected function _prepareMassaction()
    {
    	$this->setMassactionIdField('resource_id');
    	$this->getMassactionBlock()->setFormFieldName('resource_id');
		
    	$this->getMassactionBlock()->addItem('update', array(
    			'label'=> Mage::helper('modman')->__('Update'),
    			'url'  => $this->getUrl('*/*/massUpdate', array('' => '')),        // public function massDeleteAction() in Mage_Adminhtml_Tax_RateController
    			'confirm' => Mage::helper('modman')->__('Are you sure?')
    	));
        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('modman')->__('Delete'),
            'url'  => $this->getUrl('*/*/massDelete', array('' => '')),        // public function massDeleteAction() in Mage_Adminhtml_Tax_RateController
            'confirm' => Mage::helper('modman')->__('Are you sure?')
        ));
		
//    	$this->getMassactionBlock()->addItem('deploy', array(
//    			'label'=> Mage::helper('modman')->__('Deploy'),
//    			'url'  => $this->getUrl('*/*/massDeploy', array('' => '')),        // public function massDeleteAction() in Mage_Adminhtml_Tax_RateController
//    			'confirm' => Mage::helper('modman')->__('Are you sure?')
//    	));
		
    	$this->getMassactionBlock()->addItem('exporttofolder', array(
    			'label'=> Mage::helper('modman')->__('Export to Folder'),
    			'url'  => $this->getUrl('*/*/massExport', array('' => ''))
    	));

        $this->getMassactionBlock()->addItem('convert', array(
            'label'=> Mage::helper('modman')->__('Convert Modul'),
            'url'  => $this->getUrl('*/*/massConvert', array('' => ''))
        ));

//    	$this->getMassactionBlock()->addItem('reset', array(
//    			'label'=> Mage::helper('modman')->__('Git Reset'),
//    			'url'  => $this->getUrl('*/*/massReset', array('' => '')),        // public function massDeleteAction() in Mage_Adminhtml_Tax_RateController
//    			'confirm' => Mage::helper('modman')->__('Are you sure?')
//    	));
    	return $this;
    }
    
}