<?php

class Highdigital_Modman_Block_Adminhtml_Modman_New extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		parent::__construct();

		$this->_objectId = 'resource_id';
		$this->_blockGroup = 'modman';
		$this->_controller = 'adminhtml_modman';
		$this->_mode = 'new';

	}

	public function getHeaderText()
	{
	
		return Mage::helper('modman')->__('New Modman Modul');
	}
	
	protected function _prepareLayout()
	{
		if ($this->_blockGroup && $this->_controller && $this->_mode) {
			$this->setChild('form', $this->getLayout()->createBlock($this->_blockGroup . '/' . $this->_controller . '_' . $this->_mode . '_form'));
		}
		return parent::_prepareLayout();
	}

}