<?php
class Highdigital_Modman_Block_Adminhtml_Modman_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {

        $value =  $row->getData($this->getColumn()->getIndex());

        $status = array(
            'behind'    =>  'orange',
            'ahead'     =>  'blue',
            'identical' =>  'green',
            'error'     =>  'red',
            'notice'    =>  'black'

        );

        return '<span style="color:'.$status[$value].';">'.$row->getData('message').'</span>';
    }

}