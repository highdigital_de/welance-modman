<?php

class Highdigital_Modman_Block_Adminhtml_Modman_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		parent::__construct();

		$this->_objectId = 'resource_id';
		$this->_blockGroup = 'modman';
		$this->_controller = 'adminhtml_modman';
		$this->_mode = 'edit';

		// $this->_addButton('save_and_continue', array(
				// 'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
				// 'onclick' => 'saveAndContinueEdit()',
				// 'class' => 'save',
		// ), -100);
		$this->_updateButton('save', 'label', Mage::helper('modman')->__('Save Modul and Deploy'));
		// $this->_removeButton('save');
		// $this->_addButton('update_modul', array(
			// 'label' => Mage::helper('modman')->__('Save Modul and Deploy'),
            // 'onclick'   => "setLocation('".$this->getUrl('*/*/update/resource_id/'. Mage::registry('modman_data')->getResourceId())."')",
            // 'class'     => 'task'
        // ), -100);
		$this->_addButton('reset_modul', array(
			'label' => Mage::helper('modman')->__('Reset Modul'),
            'onclick'   => "setLocation('".$this->getUrl('*/*/reset/resource_id/'. Mage::registry('modman_data')->getResourceId())."')",
            'class'     => 'task'
        ), -100);

		// $this->_formScripts[] = "
            // function toggleEditor() {
                // if (tinyMCE.getInstanceById('form_content') == null) {
                    // tinyMCE.execCommand('mceAddControl', false, 'edit_form');
                // } else {
                    // tinyMCE.execCommand('mceRemoveControl', false, 'edit_form');
                // }
            // }
// 
            // function saveAndContinueEdit(){
                // editForm.submit($('edit_form').action+'back/edit/');
            // }
        // ";
	}

	public function getHeaderText()
	{
		if (Mage::registry('modman_data') && Mage::registry('modman_data')->getResourceId())
		{
			return Mage::helper('modman')->__('Edit Modul "%s"', $this->htmlEscape(Mage::registry('modman_data')->getModmanName()));
		} else {
			return Mage::helper('modman')->__('New Modman Modul');
		}
	}
	
	protected function _prepareLayout()
	{
		if ($this->_blockGroup && $this->_controller && $this->_mode) {
			$this->setChild('form', $this->getLayout()->createBlock($this->_blockGroup . '/' . $this->_controller . '_' . $this->_mode . '_form'));
		}
		return parent::_prepareLayout();
	}

}