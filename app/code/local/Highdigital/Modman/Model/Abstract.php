<?php

class highdigital_modman_Model_Abstract extends Mage_Core_Model_Abstract{

    private  $token = 'de7d19ba92c16a967e97f71b5dafbf41604e2955';
    private  $apiUrlGitHub = 'https://api.github.com/';


    private function getWebUser(){
        if(function_exists('posix_getpwuid')){
            $processUser = posix_getpwuid(posix_geteuid());
            $currentUser = $processUser['name'];
        } else if($_GET['webuser']){

            $currentUser = $_GET['webuser'];
        } else {

            Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('modman')->__('no webuser defiend'));

        }

        return $currentUser;

    }

    public function getHomedir() {

        $currentUser = $this->getWebUser();
        exec('echo ~'.$currentUser,$userHomeDirectory);
        $userHomeDir = $userHomeDirectory[0].DS;

        return $userHomeDir;

    }
    public function getGithubKeyExists(){

        $localKey = file_get_contents($this->publicKeyPath());
        $token = $this->token;
        $_apiBaseUrl = $this->apiUrlGitHub;
        $_apiFunctionPath = 'user/keys';
        $exists = false;

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $_apiBaseUrl.$_apiFunctionPath);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array('Authorization: token '.$token));
        curl_setopt($curl, CURLOPT_USERAGENT,'Highdigital-Admin-App');
        $response = curl_exec($curl);

        // close curl resource to free up system resources
        curl_close($curl);
        $keys = json_decode($response, true);
        foreach($keys as $key){

            if(strpos($localKey,$key['key']) !== false){
                $exists = true;

            }

        }
        return $exists;

    }
    public function setGithubPublicKey(){

        $token = $this->token;
        $_apiBaseUrl = $this->apiUrlGitHub;
        $_apiFunctionPath = 'user/keys';


        $currentUser = get_current_user();
        exec('echo ~'.$currentUser,$userHomeDir);
        $userHomeDir = $userHomeDir[0].DS;

        if(!is_dir($userHomeDir.'.ssh')){

            mkdir($userHomeDir.'.ssh');
        }

        $publiykey_filename = 'id_rsa.pub';

        if(Mage::getStoreConfig('modman/config/init_pubkey_filename')){
            $publiykey_filename = Mage::getStoreConfig('modman/config/init_pubkey_filename');
        }

        $keydata = array(
            "title"=> gethostname().'-'.$_SERVER['SERVER_ADDR'],
            "key"=> file_get_contents($userHomeDir.'/.ssh/'.$publiykey_filename)
        );
        $keydata = json_encode($keydata);
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $_apiBaseUrl.$_apiFunctionPath);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array('Authorization: token '.$token));
        curl_setopt($curl, CURLOPT_USERAGENT,'Highdigital-Admin-App');
        curl_setopt($curl,CURLOPT_POST, count($keydata));
        curl_setopt($curl,CURLOPT_POSTFIELDS, $keydata);
        $response = curl_exec($curl);

        // close curl resource to free up system resources
        curl_close($curl);

        $result = json_decode($response, true);

        if(array_key_exists('verified',$result) && $result['verified']){

            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('modman')->__('Pubkey wurde bei GitHub angelegt'));


        } else if(array_key_exists('errors',$result) && $result['errors'][0]['message'] == 'key is already in use'){

            Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('modman')->__('Pubkey existiert schon bei GitHub'));


        } else {

            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('modman')->__('Pubkey konnte nicht bei GitHub angelegt werden'));


        }

    }

    public function publicKeyPath(){

        $currentUser = get_current_user();
        $publiykey_filename = 'id_rsa.pub';

        if(Mage::getStoreConfig('modman/config/init_pubkey_filename')){
            $publiykey_filename = Mage::getStoreConfig('modman/config/init_pubkey_filename');
        }

        exec('echo ~'.$currentUser,$userHomeDir);

        return $userHomeDir[0].DS.'.ssh/'.$publiykey_filename;
    }

    public function privateKeyPath(){

        $currentUser = get_current_user();
        $privatekey_filename = 'id_rsa';

        if(Mage::getStoreConfig('modman/config/init_privatekey_filename')){
            $privatekey_filename = Mage::getStoreConfig('modman/config/init_privatekey_filename');
        }

        exec('echo ~'.$currentUser,$userHomeDir);

        return $userHomeDir[0].DS.'.ssh/'.$privatekey_filename;
    }
    public function githubRepoExists($githubPath){

        $result = false;
        $githubUserRepo = $this->_getGithubUserRepo($githubPath);

        if(strpos(strtolower($githubPath),'github.com')) {
            $token = $this->token;
            $_apiBaseUrl = $this->apiUrlGitHub;
            $_apiFunctionPath = 'repos/' . $githubUserRepo;
            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $_apiBaseUrl . $_apiFunctionPath);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: token ' . $token));
            curl_setopt($curl, CURLOPT_USERAGENT, 'Highdigital-Admin-App');
            $response = curl_exec($curl);

            // close curl resource to free up system resources
            curl_close($curl);
            $keys = json_decode($response, true);

            if(array_key_exists('id',$keys)){
                $result = true;
            }
        }

        return $result;

    }
    public function githubCompareBranches($githubPath, $localKey = null){

        $result = array();

        $modulname = $this->__getModmanName($githubPath);
        $githubUserRepo = $this->_getGithubUserRepo($githubPath);

        if(strpos(strtolower($githubPath),'github.com')) {
            $token = $this->token;
            $_apiBaseUrl = $this->apiUrlGitHub;
            $revNumber = trim($this->_getGitHEADId($modulname));
            $branch = trim($this->_getGitBranch($modulname));
            $_apiFunctionPath = 'repos/' . $githubUserRepo . '/compare/' . $revNumber . '...' . $branch;
            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $_apiBaseUrl . $_apiFunctionPath);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: token ' . $token));
            curl_setopt($curl, CURLOPT_USERAGENT, 'Highdigital-Admin-App');
            $response = curl_exec($curl);

            // close curl resource to free up system resources
            curl_close($curl);
            $keys = json_decode($response, true);

            if (is_array($keys) && array_key_exists('status', $keys) && $keys['status'] != 'identical') {
                if ($keys['status'] == 'behind') {
                    $status = 'ahead';
                } else if ($keys['status'] == 'ahead') {
                    $status = 'behind';
                }
                $status_by = $keys[$keys['status'] . '_by'];
                $result['message'] = Mage::helper('modman')->__('%s is %s by %s commit(s)', $modulname, $status, $status_by);
                $result['status'] = $status;
                if ($modulname == 'highdigital-modman') {

                    Mage::getSingleton('adminhtml/session')->addNotice($result['message'].' '.Mage::helper('modman')->__('Update <a href="'.Mage::helper('adminhtml')->getUrl('*/*/updateModman/').'" title="update now!">now</a>?'));
                }
            } else if (is_array($keys) && array_key_exists('status', $keys) && $keys['status'] == 'identical') {

                $result['message'] = Mage::helper('modman')->__('%s is up to date', $modulname);
                $result['status'] = 'identical';
                if ($modulname == 'highdigital-modman') {
                    Mage::getSingleton('adminhtml/session')->addSuccess($result['message']);
                }
            } else {

                $result['message'] = Mage::helper('modman')->__('%s an error accure', $modulname);
                $result['status'] = 'error';
                if ($modulname == 'highdigital-modman') {
                    Mage::getSingleton('adminhtml/session')->addError($result['message']);
                }
            }
        } else {

            $result['message'] = Mage::helper('modman')->__('%s is not hosted on Github', $modulname);
            $result['status'] = 'notice';
            if ($modulname == 'highdigital-modman') {
                Mage::getSingleton('adminhtml/session')->addError($result['message']);
            }
        }

        return $result;

    }


    public function gitCompareBranches($remoteUrl){

        $result = array();
        if(strstr($remoteUrl,'github.com:highdigital')){

            return $this->githubCompareBranches($remoteUrl);

        }
        $modulname = $this->__getModmanName($remoteUrl);
        try{
            chdir(Mage::getBaseDir('base').'/.modman/'. $modulname);
            exec('git fetch');
            exec('git status -sb', $execResult, $return);
            if(preg_match('/\[(.*?)\]/', $execResult[0], $match)) {

                $result['message'] = Mage::helper('modman')->__('%s is %s commit(s).', $modulname, $match[1]);
                $result['status'] = 'not identical';

            } else {
                $result['message'] = Mage::helper('modman')->__('%s is uptodate', $modulname);
                $result['status'] = 'identical';
            }

        }
        catch(Exception $e){
            Mage::logException($e);
        }

        return $result;

    }

    public function _getGitHEADId($modmanName = false){

        if ($modmanName == false){

//            $modmanName = $this->_getModmanName();

        }

        try{
            chdir(Mage::getBaseDir('base').'/.modman/'. $modmanName);
            $git_version = shell_exec('git rev-parse HEAD');

            $this->setData('git_revision',$git_version);
            return $git_version;

        }
        catch(Exception $e){
            Mage::logException($e);
        }
    }

    public function _getGitBranch($modmanName = false){

        if ($modmanName == false){

            //$modmanName = $this->_getModmanName();

        }

        try{
            chdir(Mage::getBaseDir('base').'/.modman/'. $modmanName);
            $branch = shell_exec('git rev-parse --abbrev-ref HEAD');

            return $branch;

        }
        catch(Exception $e){
            Mage::logException($e);
        }
    }
    public function __getModmanName($repoPath){

        $gitUrl = parse_url(str_replace('.git','',strtolower($repoPath)));

        if(array_key_exists('scheme',$gitUrl)){
            $repoName = basename($gitUrl['path']);

        } else {

            preg_match('/:(.*)/', $gitUrl['path'], $matches);
            $repoName =  end($matches);
            $repoName = basename($repoName);

        }

        return $repoName;

    }
    public function _getGithubUserRepo($repoPath){

        $gitUrl = parse_url(str_replace('.git','',strtolower($repoPath)));
        if(array_key_exists('scheme',$gitUrl)){

            $repoName = substr($gitUrl['path'],1);

        } else {

            preg_match('/:(.*)/', $gitUrl['path'], $matches);

            $repoName =  end($matches);

        }

        return $repoName;

    }


}