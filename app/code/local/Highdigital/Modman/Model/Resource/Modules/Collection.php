<?php

/**
 * @author Dima Makaruk 
 */

class Highdigital_Modman_Model_Resource_Modules_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract{
    protected function _construct(){
        $this->_init('modman/modules');
    }
  
}