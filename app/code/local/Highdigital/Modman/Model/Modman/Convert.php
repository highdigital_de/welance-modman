<?php
/**
 * Created by PhpStorm.
 * User: ngongoll
 * Date: 25.02.16
 * Time: 16:33
 */

class Highdigital_Modman_Model_Modman_Convert extends Highdigital_Modman_Model_Abstract{

    public function _convertRepos(){
        $collection = Mage::getModel('modman/modules')->getCollection();

        foreach($collection as $repo){
            $this->_convert($repo);
        }

    }

    public function _convert($repo){
        $result = false;
        $remoteUrl = $repo->getData('git_clone_url');
        if(strpos($remoteUrl,'@github.com:highdigital')){

            if(!$this->githubRepoExists('git@bitbucket.org:highdigital_de/'.$repo->getData('modman_name').'.git')){


                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('modman')->__(Mage::helper('modman')->__("%s cannot be converted because no Bitbucket Repo exists",$repo->getData('modman_name'))));

            } else {

                chdir(Mage::getBaseDir('base').'/.modman/'. $repo->getData('modman_name'));
                exec('git remote rm origin', $log, $return);
                exec('git remote add origin bitbucket.org:highdigital_de/'.$repo->getData('modman_name').'.git', $log, $return);
                exec('git branch --track  2>&1', $log, $return);
                $branches = $log;
                foreach($branches as $branch){
                    unset($log);

                    exec('git branch --set-upstream '.$repo->getData('git_branch').' origin/'.$repo->getData('git_branch'). ' 2>&1', $log, $return);
                    if(strstr($log[0],'fatal: Not a valid object name:')){
                        unset($log);
                        exec('git fetch origin  2>&1', $log, $return);
                        exec('git branch --set-upstream '.$repo->getData('git_branch').' origin/'.$repo->getData('git_branch'). ' 2>&1', $log, $return);
                    }
                }
                $repo->setData('git_clone_url','git@bitbucket.org:highdigital_de/'.$repo->getData('modman_name').'.git');
                $repo->save();

                $result = true;

            }
        } else {
            throw new Mage_Core_Exception(
                Mage::helper('modman')->__("%s cannot be converted",$repo->getData('modman_name'))
            );

        }

        return $result;


    }


}