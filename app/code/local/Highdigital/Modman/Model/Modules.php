<?php

class Highdigital_Modman_Model_Modules extends Highdigital_Modman_Model_Abstract{

    const BASEDIR_PATH = '.modman/.basedir';
    const EXPORT_PATH = "var/modman_export";

    private  $token = 'de7d19ba92c16a967e97f71b5dafbf41604e2955';
    private  $apiUrlGitHub = 'https://api.github.com/';

    protected function _construct(){
        $this->_init('modman/modules');
        return $this;
    }

    public function addModul(){

        if($this->cloneModul())
        {
            // throw new Mage_Core_Exception(
            // $this->__($result)
            // );
        }

    }
    public function _initProject(){

        $projects = Mage::getStoreConfig('modman/repos');

        foreach($projects as $repo){

            $sourceName = str_replace('.git','',strtolower($repo['source']));
            preg_match('/:(.*)/', $sourceName, $matches);
            $repoName =  $matches[1];
            if(empty($sourceName)){
                continue;
            }
            if(Mage::getModel('modman/modules')->load($repoName,'modman_name')->getData()  === array()){

                $model = Mage::getModel('modman/modules');
                $model->setData('git_clone_url',$repo['source']);
                $model->setData('git_branch',$repo['branch']);

            } else {

                $model = Mage::getModel('modman/modules')->load($repoName,'modman_name');


            }

            try{

                $model->cloneModul();

                $model->setData('modman_name',$repoName);
                Mage::getSingleton('adminhtml/session')->addSuccess($model->_getModmanName().Mage::helper('modman')->__(' successfully cloned'));

            } catch (Exception $e){

                if(strpos($e->getMessage(),'has already been checked out')){
                    $model->setData('modman_name',$repoName);

                    $model->update();
                    Mage::getSingleton('adminhtml/session')->addSuccess($model->_getModmanName().Mage::helper('modman')->__(' successfully updated'));



                } else {


                    Mage::getSingleton('adminhtml/session')->addError($model->getData('status_msg'));

                    continue;

                }

            }
            try{

                $model->save();

            } catch (Exception $e){

                Mage::logException($e);
            }

        }


    }

    public function deploy($copy = false){

        try{
            exec(Mage::Helper('modman')->modmanCmd().'deploy '. $this->_getModmanName().' --force'.($copy?' --copy':'').' 2>&1', $result, $return);
        }
        catch(Exception $e){
            print_r($e->getMessage());
            Mage::logException($e);
        }
        $result = implode('<br/>',$result);


        if(strstr($result,'Run with --force') && strstr(strtoupper($result),'CONFLICT')){

            try{
                exec(Mage::Helper('modman')->modmanCmd().'deploy '. $this->_getModmanName().' --force', $result, $return);
            }
            catch(Exception $e){
                Mage::logException($e);
            }
            $result = implode('<br/>',$result);


        }

        if(strstr(strtoupper($result),'ERROR') || strstr(strtoupper($result),'CONFLICT')){

        } else{

            $this->setData('status',$this->_getModmanName().' is up to date');
        }

    }

    public function remove(){

        try{
            exec(Mage::Helper('modman')->modmanCmd().'remove '. $this->_getModmanName(), $result, $return);
        }
        catch(Exception $e){
            Mage::logException($e);
        }
        $result = implode('<br/>',$result);

        if(strstr(strtoupper($result),'ERROR') || strstr(strtoupper($result),'CONFLICT')){

            $this->setData('status','remove_error');
        } else{

            $this->setData('status','removed');
        }

    }


    public function export(){

        $filename = self::BASEDIR_PATH;
        $export_basedir = self::EXPORT_PATH;
        $basedirExists = true;

        if(!file_exists($filename)){
            if (!$handle = fopen($filename, "r")) {
                $this->createBasedir();
                $basedirExists = false;
            }
        }
        if(!is_dir($export_basedir)){

            mkdir($export_basedir, 0777, true);
        }
        // Sichergehen, dass die Datei existiert und beschreibbar ist
        if (is_writable($filename)) {

            $org_basedir = file_get_contents($filename);

            $handle = fopen($filename, "w");

            // Schreibe $somecontent in die geöffnete Datei.
            if (!fwrite($handle, $export_basedir)) {

                throw new Mage_Core_Exception(
                    "Kann in die Datei $filename nicht schreiben"
                );
            }

            fclose($handle);


        } else {
            throw new Mage_Core_Exception(
                "Die Datei $filename ist nicht schreibbar"
            );
        }
        try{
            exec(Mage::Helper('modman')->modmanCmd().'deploy '. $this->_getModmanName().' --force --copy', $result, $return);
            if($basedirExists){
                $this->setBasedir($org_basedir);
            } else {
                unlink($filename);
            }
        }
        catch(Exception $e){

            $this->setBasedir($org_basedir);
            Mage::logException($e);
        }
        $resultString = implode('<br/>',$result);


        if(strstr(strtoupper($result),'ERROR') || strstr(strtoupper($result),'CONFLICT')){

            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('modman')->__($resultString).' ('.$this->_getModmanName().')');
            $this->setData('status','export_error');
        } else{

            $this->setData('status','good');
        }

    }

    public function setBasedir($path='')
    {

        $filename = self::BASEDIR_PATH;
        if (!$handle = fopen($filename, "w")) {
            throw new Mage_Core_Exception(
                "Kann die Datei $filename nicht öffnen"
            );
        }

        if (!fwrite($handle, $path)) {

            throw new Mage_Core_Exception(
                "Kann in die Datei $filename nicht schreiben 2"
            );
        }

        fclose($handle);
    }

    public function createBasedir()
    {

        $path = self::EXPORT_PATH;
        $filename = self::BASEDIR_PATH;
        if (!$handle = fopen($filename, "w")) {
            throw new Mage_Core_Exception(
                "Kann die Datei $filename nicht öffnen"
            );
        }

        if (!fwrite($handle, $path)) {

            throw new Mage_Core_Exception(
                "Kann in die Datei $filename nicht schreiben 2"
            );
        }

        fclose($handle);
    }

    public function update(){

        try{
            unset($result);
            exec(Mage::Helper('modman')->modmanCmd().'update '. $this->_getModmanName().' --force --Branch '.$this->getGitBranch() . '  2>&1', $result);
        }
        catch(Exception $e){
            Mage::logException($e);
        }
        $resultString = implode('<br/>',$result);

        Mage::getSingleton('adminhtml/session')->addNotice($resultString);
        if(strstr(strtoupper($resultString),'Update of '.$this->_getModmanName().' complete.') && (strstr(strtoupper($resultString),'FAILED') || strstr(strtoupper($resultString),'ERROR') || strstr(strtoupper($resultString),'CONFLICT'))){

            //Mage::getSingleton('adminhtml/session')->addError(Mage::helper('modman')->__($resultString).' ('.$this->_getModmanName().')');
//            $this->setData('status','update_error');
            $this->setData('status_msg',$resultString);

        } else{

            //Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('modman')->__($resultString).' ('.$this->_getModmanName().')');

            $this->setData('git_revision',$this->_getGitHEADId($this->_getModmanName()));
            $this->setData('commit_date',$this->_getCommitDate($this->_getModmanName()));
            $this->setData('status','identical');
            $this->setData('message',$this->_getModmanName() . ' is up to date');
            $this->setData('last_update',date('Y.m.d'));
        }

    }


    public function gitreset(){

        try{

            chdir($_SERVER['DOCUMENT_ROOT']);
            chdir(Mage::getBaseDir('base').'/.modman/'. $this->_getModmanName());
            exec('git reset --hard HEAD', $result, $return);

        }
        catch(Exception $e){
            Mage::logException($e);
        }

        $resultString = implode('<br/>',$result);
        if(strstr($resultString,'Error') || strstr($resultString,'fatal')){

            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('modman')->__($resultString).' ('.$this->_getModmanName().')');

        } else {

            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('modman')->__($result[0]).' ('.$this->_getModmanName().')');
        }
        return $result;

    }
    public function cloneModul(){

        exec(Mage::Helper('modman')->modmanCmd().'clone '. $this->_getModmanName().' '. $this->getGitCloneUrl(). ($this->getGitBranch()?' --branch "'.$this->getGitBranch():'').'"  2>&1', $result);

        $resultString = implode('<br/>',$result);
        if(strpos($resultString,'has already been checked out') || strpos($resultString,'Error')){

            $this->setData('status','clone error');
            $this->setData('status_msg',$resultString);

            throw new Mage_Core_Exception(
                Mage::helper('modman')->__($resultString)
            );
        } else {

            $this->setData('last_update',date('Y.m.d'));
            $this->setData('git_revision',$this->_getGitHEADId($this->_getModmanName()));
            $this->setData('modman_name',$this->_getModmanName());
            $this->setData('status','identical');
            $this->setData('message',$this->_getModmanName() . ' is up to date');

        }

        return true;

    }
    public function updateModul(){
        try{
            exec(Mage::Helper('modman')->modmanCmd().'clean '. $this->_getModmanName(), $result, $return);
            if($this->_getModmanName()){

                exec('rm -R '.Mage::getBaseDir('base').'/.modman/'. $this->_getModmanName(), $result, $return);

            }
        }
        catch(Exception $e){
            Mage::logException($e);
        }

        exec(Mage::Helper('modman')->modmanCmd().'clone '. $this->_getModmanName().' '. $this->getGitCloneUrl(). ($this->getGitBranch()?' --branch "'.$this->getGitBranch().'"':''),$result,$return);
        $resultString = implode('<br/>',$result);
        if(strpos($resultString,'has already been checked out')){

            throw new Mage_Core_Exception(
                Mage::helper('modman')->__($resultString)
            );
        } else {

            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('modman')->__($resultString));
            $this->setData('last_update',date('Y.m.d'));
            $this->setData('git_revision',$this->_getGitHEADId($this->_getModmanName()));
            $this->setData('modman_name',$this->_getModmanName());
            $this->setData('status','identical');
            $this->setData('message',$this->_getModmanName() . ' is up to date');
            $this->setData('commit_date',$this->_getCommitDate($this->_getModmanName()));

        }

    }
    protected function _getGitStatus(){


        $git_version = shell_exec('git --version');

        return $git_version;
    }

    public function deleteModul(){


        if($this->_getModmanName() && $this->_getModmanName() != '*' )	{

            Mage::log('deleteModul',null,'modman.log',true);
            try{
                exec('rm -R '.Mage::getBaseDir('base').'/.modman/'. $this->_getModmanName(), $result, $return);
                exec(Mage::Helper('modman')->modmanCmd().'clean '. $this->_getModmanName(), $result, $return);
            }
            catch(Exception $e){
                Mage::logException($e);
            }
            try{
                $this->delete();
            }
            catch(Exception $e){
                Mage::logException($e);
            }
        }
    }

    public function _initPublicKey(){
        $currentUser = get_current_user();
        exec('echo ~'.$currentUser,$userHomeDir);
        $userHomeDir = $userHomeDir[0].DS;

        if(!is_dir($userHomeDir.'.ssh')){

            mkdir($userHomeDir.'.ssh');
        }

        if(file_exists($userHomeDir.'.ssh/id_rsa.pub')== false){
            exec('ssh-keygen -t rsa -P "" -f '.$userHomeDir.'.ssh/id_rsa',$log,$result);

            exec('chown -R '.$currentUser.':'. $currentUser .' '.$userHomeDir.'.ssh',$log,$result);
            exec('chmod 0700 .ssh',$log,$result);
            exec('chmod 0600 .ssh/id_rsa',$log,$result);
            if(file_exists($userHomeDir.'.ssh/id_rsa.pub') == false){

                $log = implode('<br/>',$log);
                throw new Mage_Core_Exception(
                    $this->__($log)
                );
            }
        }

        return file_exists($userHomeDir.'.ssh/id_rsa.pub');
    }


    public function _saveKeys($keydata){
        $currentUser = get_current_user();
        exec('echo ~'.$currentUser,$userHomeDir);
        $userHomeDir = $userHomeDir[0].DS;

        if(!is_dir($userHomeDir.'.ssh')){

            mkdir($userHomeDir.'.ssh');
        }

        if(file_exists($userHomeDir.'.ssh/id_rsa.pub')== false){

            file_put_contents($userHomeDir.'.ssh/id_rsa', $keydata['privatekey']);
            file_put_contents($userHomeDir.'.ssh/id_rsa.pub', $keydata['publickey']);

            exec('chmod 0700 .ssh',$log,$result);
            exec('chmod 0600 .ssh/id_rsa',$log,$result);

            if(file_exists($userHomeDir.'.ssh/id_rsa.pub') == false){

                $log = implode('<br/>',$log);
                throw new Mage_Core_Exception(
                    $this->__($log)
                );
            }
        }

        return file_exists($userHomeDir.'.ssh/id_rsa.pub');
    }

    public function publicKeyExits(){

        return file_exists($this->publicKeyPath());
    }



    public function setGithubPublicKey(){

        $token = $this->token;
        $_apiBaseUrl = $this->apiUrlGitHub;
        $_apiFunctionPath = 'user/keys';


        $currentUser = get_current_user();
        exec('echo ~'.$currentUser,$userHomeDir);
        $userHomeDir = $userHomeDir[0].DS;

        if(!is_dir($userHomeDir.'.ssh')){

            mkdir($userHomeDir.'.ssh');
        }

        $publiykey_filename = 'id_rsa.pub';

        if(Mage::getStoreConfig('modman/config/init_pubkey_filename')){
            $publiykey_filename = Mage::getStoreConfig('modman/config/init_pubkey_filename');
        }

        $keydata = array(
            "title"=> gethostname().'-'.$_SERVER['SERVER_ADDR'],
            "key"=> file_get_contents($userHomeDir.'/.ssh/'.$publiykey_filename)
        );
        $keydata = json_encode($keydata);
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $_apiBaseUrl.$_apiFunctionPath);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array('Authorization: token '.$token));
        curl_setopt($curl, CURLOPT_USERAGENT,'Highdigital-Admin-App');
        curl_setopt($curl,CURLOPT_POST, count($keydata));
        curl_setopt($curl,CURLOPT_POSTFIELDS, $keydata);
        $response = curl_exec($curl);

        // close curl resource to free up system resources
        curl_close($curl);

        $result = json_decode($response, true);

        if($result['verified']){

            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('modman')->__('Pubkey wurde bei GitHub angelegt'));


        } else if(array_key_exists('errors',$result) && $result['errors'][0]['message'] == 'key is already in use'){

            Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('modman')->__('Pubkey existiert schon bei GitHub'));


        } else {

            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('modman')->__('Pubkey konnte nicht bei GitHub angelegt werden'));


        }

    }

    public function sshConfigExits(){

        $currentUser = get_current_user();
        exec('echo ~'.$currentUser,$userHomeDir);

        return file_exists($userHomeDir[0].DS.'.ssh/config');
    }

    public function _initKnownHosts(){
        $gitUrl = 'projekt.highdigital.de';
        $currentUser = get_current_user();
        exec('echo ~'.$currentUser,$userHomeDir);
        exec('ssh-keyscan -H -t ecdsa '.$gitUrl.' >> '.$userHomeDir[0].'/.ssh/known_hosts',$report,$return);
        return $report;
    }

    public function _initConfig(){

        $currentUser = get_current_user();
        exec('echo ~'.$currentUser,$userHomeDir);
        $userHomeDir = $userHomeDir[0].DS;

        if(!is_dir($userHomeDir.'.ssh')){

            mkdir($userHomeDir.'.ssh');
        }

        if(file_exists($userHomeDir.'.ssh/config') == false){

            exec('touch '.$userHomeDir.'.ssh/config');
            exec('echo "ServerAliveInterval 60

Host *
ForwardAgent yes
StrictHostKeyChecking no
IdentityFile    '. Mage::getModel('modman/modules')->privateKeyPath() .'" > '.$userHomeDir.'.ssh/config');

            exec('chmod 0700 '.$userHomeDir.'.ssh/config',$log,$result);
        }
    }


    public function _getCommitDate($modmanName = false){
        if ($modmanName == false){

            $modmanName = $this->_getModmanName();

        }

        try{
            chdir(Mage::getBaseDir('base').'/.modman/'. $modmanName);
            $date = date('Y.m.d',shell_exec('git show -s --format=%ct'));

            $this->setData('commit_date',$date);

            return $date;

        }
        catch(Exception $e){
            Mage::logException($e);


        }
    }
    public function _getModmanName(){

        $gitUrl = parse_url(str_replace('.git','',strtolower($this->getData('git_clone_url'))));

        if(array_key_exists('scheme',$gitUrl)){
            $repoName = basename($gitUrl['path']);

        } else {

            preg_match('/:(.*)/', $gitUrl['path'], $matches);
            $repoName =  end($matches);
            $repoName = basename($repoName);

        }

        return $repoName;

    }
}