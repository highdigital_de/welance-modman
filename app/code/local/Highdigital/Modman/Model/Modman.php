<?php

class Highdigital_Modman_Model_Modman extends Highdigital_Modman_Model_Abstract{

    public function checkForUpdate(){

        if(!$this->getGithubKeyExists()){
            $this->setGithubPublicKey();
        }

        $this->githubCompareBranches('git@github.com:highdigital/highdigital-modman.git');

    }


}