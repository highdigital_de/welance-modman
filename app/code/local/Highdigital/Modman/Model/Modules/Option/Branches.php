<?php
 
class Highdigital_Modman_Model_Modules_Option_Branches extends Varien_Object
{
 
 
    protected $_options = array();
 
    /**
     * @param int $parentId
     * @param int $recursionLevel
     *
     * @return array
     */
    public function getOptionArray()
    {
        
 		return $this->_remoteBranches();
    }
	
	
	
	protected function _remoteBranches(){
        $_options = array();
		try{
    		chdir(Mage::getBaseDir('base').'/.modman/'.Mage::registry('modman_data')->getModmanName());
            $remoteBranches = shell_exec('git ls-remote --heads origin');
            $branches = explode(chr(10),$remoteBranches);

			foreach($branches as $branch){
                if(empty($branch)){
                    continue;
                }
				$branchObj = preg_split('/\s+/',$branch);
                $branchName  = substr( $branchObj[1], strrpos( $branchObj[1], '/' )+1 );
				if($branchName == 'HEAD' || empty($branchName))
				{
					continue;
				}
				$_options[] = array(
 
                'label' => $branchName,
                'value' => $branchName
           		);
			}

			return $_options;
		
		}
			catch(Exception $e){
            Mage::logException($e);
		}
	}
   }	