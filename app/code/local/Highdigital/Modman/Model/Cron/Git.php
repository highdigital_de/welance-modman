<?php
/**
 * Created by PhpStorm.
 * User: ngongoll
 * Date: 23.11.17
 * Time: 11:24
 */

class Highdigital_Modman_Model_Cron_Git extends  Mage_Core_Model_Abstract{

    public function checkGitStatus(){

        $repoCollection = Mage::getModel('modman/modules')->getCollection();

        foreach ($repoCollection as $item) {

            $statusResponse = Mage::getModel('modman/modman')->gitCompareBranches($item->getGitCloneUrl());
            $item->setData('status',$statusResponse['status']);
            $item->setData('message',$statusResponse['message']);
            $item->save();

        }


    }

}