<?php
/**
 * Created by PhpStorm.
 * User: ngongoll
 * Date: 04.05.15
 * Time: 10:30
 */

class Highdigital_Modman_Model_Observer extends Mage_Core_Model_Abstract {


    public function updateModmanModul(){


        exec(Mage::getBaseDir('base').'/modman update git@projekt.highdigital.de:highdigital-modman.git 2>&1', $result);

        $resultString = implode('<br/>',$result);

        if(strpos($resultString,'has already been checked out') || strpos($resultString,'Error trying to clone')){

            $respose =  Mage::helper('modman')->__($resultString);

        } else {

            $respose = 'update erfolgreich' ;

        }

        return $respose;

    }
    public function addGitStatusUpdate($observer)
    {
        $container = $observer->getBlock();
        if(null !== $container && $container->getType() == 'modman/adminhtml_modman') {
            $data = array(
                'label'     => 'Update GIT Status',
                'class'     => 'some-class',
                'onclick'   => 'setLocation(\' '  . Mage::helper("adminhtml")->getUrl('*/*/', array('update' => 'true')) . '\')',
            );
            $container->addButton('git_update_status', $data);
        }

        return $this;
    }


}