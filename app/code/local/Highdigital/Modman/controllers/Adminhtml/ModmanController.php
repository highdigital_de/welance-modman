<?php
 
class Highdigital_Modman_Adminhtml_ModmanController extends Mage_Adminhtml_Controller_Action
{
 
    public function indexAction()
    {
//        Mage::getModel('modman/modman')->checkForUpdate();
        if( $this->getRequest()->getParam('update', null) == true){

            Mage::getModel('modman/cron_git')->checkGitStatus();
        }
        $this->loadLayout();
        $this->renderLayout();
    }
 
    public function newAction()
    {
        $id = $this->getRequest()->getParam('resource_id', null);
        $model = Mage::getModel('modman/modules');
        Mage::register('modman_data', $model);
 
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->renderLayout();
    }
 
    public function editAction()
    {
        $id = $this->getRequest()->getParam('resource_id', null);
        $model = Mage::getModel('modman/modules');
        if ($id) {
            $model->load((int) $id);
            if ($model->getId()) {
                $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
                if ($data) {
                    $model->setData($data)->setId($id);
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('modman')->__('Example does not exist'));
                $this->_redirect('*/*/');
            }
        }
        Mage::register('modman_data', $model);
 
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->renderLayout();
    }
 
   public function updateAction()
    {

        if ($data = $this->getRequest()->getPost())
        {
            $model = Mage::getModel('modman/modules');
            $id = $this->getRequest()->getParam('resource_id');
            if ($id) {
                $model->load($id);
            }
			$data['status'] = 'deployed';
            $model->setData($data);
            Mage::getSingleton('adminhtml/session')->setFormData($data);
            try {
                if ($id) {
                    $model->setResourceId($id);
                }
                $model->deploy();
                $model->remove();
                $model->cloneModul();
                $model->deploy(true);
				
				//$gitRevision['git_revision'] = $model->_getGitHEADId();
				
            	$model->setData('git_revision',$model->_getGitHEADId());
                $model->save();
 
                if (!$model->getResourceId()) {
                    Mage::throwException(Mage::helper('modman')->__('Error saving Modul'));
                }
 
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('modman')->__('Modul was successfully updated.'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
 
                // The following line decides if it is a "save" or "save and continue"
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('resource_id' => $model->getResourceId()));
                } else {
                    $this->_redirect('*/*/');
                }
 
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                if ($model && $model->getResourceId()) {
                    $this->_redirect('*/*/edit', array('resource_id' => $model->getResourceId()));
                } else {
                    $this->_redirect('*/*/');
                }
            }
 
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('modman')->__('No data found to update'));
        $this->_redirect('*/*/');
    }
 
    public function resetAction()
    {
        $model = Mage::getModel('modman/modules');
        $id = $this->getRequest()->getParam('resource_id');
        if ($id) {
                $model->load($id);
        }
 
        try {
            if ($id) {
                    $model->setResourceId($id);
            }
            $model->gitreset();
 
            if (!$model->getResourceId()) {
                    Mage::throwException(Mage::helper('modman')->__('Error reseting Modul'));
            }
 
            Mage::getSingleton('adminhtml/session')->setFormData(false);
 
	                // The following line decides if it is a "save" or "save and continue"
	        if ($this->getRequest()->getParam('back')) {
	            $this->_redirect('*/*/edit', array('resource_id' => $model->getResourceId()));
	        } else {
            	$this->_redirect('*/*/');
            }
 
        } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
	        if ($model && $model->getResourceId()) {
	            $this->_redirect('*/*/edit', array('resource_id' => $model->getResourceId()));
	        } else {
            	$this->_redirect('*/*/');
            }
        }
 
        return;
    }
 
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost())
        {
            $model = Mage::getModel('modman/modules');
            $id = $this->getRequest()->getParam('resource_id');
            if ($id) {
                $model->load($id);
                $data['git_revision'] = $model->_getGitHEADId($model->getData('modman_name'));
            } else {

                $sourceName = str_replace('.git','',strtolower($this->getRequest()->getParam('git_clone_url')));
                preg_match('/:(.*)/', $sourceName, $matches);
                $data['modman_name'] = $matches[1];

            }
			$data['status'] = 'deployed';
            $model->setData($data);
            Mage::getSingleton('adminhtml/session')->setFormData($data);

            try {
                if ($id) {
                    $model->setResourceId($id);
                }

                $model->addModul();
                $model->deploy(true);
                //$model->_getGitHEADId();
                $model->save();
 
                if (!$model->getResourceId()) {
                    Mage::throwException(Mage::helper('modman')->__('Error saving Modul'));
                }
 
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('modman')->__('Modul was successfully saved.'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
 
                // The following line decides if it is a "save" or "save and continue"
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('resource_id' => $model->getResourceId()));
                } else {
                    $this->_redirect('*/*/');
                }
 
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                if ($model && $model->getResourceId()) {
                    $this->_redirect('*/*/edit', array('resource_id' => $model->getResourceId()));
                } else {
                    $this->_redirect('*/*/');
                }
            }
 
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('modman')->__('No data found to save'));
        $this->_redirect('*/*/');
    }
 
    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('resorce_id')) {
            try {
                $model = Mage::getModel('modman/modules');
                $model->setId($id);
                $model->deleteModul();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('modman')->__('The example has been deleted.'));
                $this->_redirect('*/*/');
                return;
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('resource_id' => $this->getRequest()->getParam('resource_id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Unable to find the example to delete.'));
        $this->_redirect('*/*/');
    }
    
    public function massDeleteAction()
    {
    	$resourceIds = $this->getRequest()->getParam('resource_id');      // $this->getMassactionBlock()->setFormFieldName('tax_id'); from Mage_Adminhtml_Block_Tax_Rate_Grid
    	if(!is_array($resourceIds)) {
    		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('modman')->__('Please select modul(es).'));
    	} else {
    		try {
    			$modmanModel = Mage::getModel('modman/modules');
    			foreach ($resourceIds as $modulId) {
    				$modmanModel->load($modulId)->delete();
    			}
    			Mage::getSingleton('adminhtml/session')->addSuccess(
    			Mage::helper('modman')->__(
    			'Total of %d record(s) were deleted.', count($resourceIds)
    			)
    			);
    		} catch (Exception $e) {
    			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
    		}
    	}
    	$this->_redirect('*/*/index');
    }
    
    public function massResetAction()
    {
    	$resourceIds = $this->getRequest()->getParam('resource_id');      // $this->getMassactionBlock()->setFormFieldName('tax_id'); from Mage_Adminhtml_Block_Tax_Rate_Grid
    	if(!is_array($resourceIds)) {
    		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('modman')->__('Please select modul(es).'));
    	} else {
    		try {
    			$modmanModel = Mage::getModel('modman/modules');
    			foreach ($resourceIds as $modulId) {

    				$modmanModel->load($modulId)->gitreset();
    			}
    			Mage::getSingleton('adminhtml/session')->addSuccess(
    			Mage::helper('modman')->__(
    			'Total of %d record(s) were reseted.', count($resourceIds)
    			)
    			);
    		} catch (Exception $e) {
    			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
    		}
    	}
    	$this->_redirect('*/*/index');
    }
    
    public function massUpdateAction()
    {
    	$resourceIds = $this->getRequest()->getParam('resource_id');      // $this->getMassactionBlock()->setFormFieldName('tax_id'); from Mage_Adminhtml_Block_Tax_Rate_Grid
    	if(!is_array($resourceIds)) {
    		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('modman')->__('Please select modul(es).'));
    	} else {
    		try {
    			foreach ($resourceIds as $modulId) {
					$data = array();
    				$model = Mage::getModel('modman/modules')->load($modulId);
                    $model->deploy();
                    $model->remove();
    				$model->cloneModul();
                    $model->deploy(true);
                    if(strpos($model->getStatus(),'error')){

                        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('modman')->__($model->getStatusMsg()).' ('.$model->getModmanName().')');
                        throw new Mage_Core_Exception(
                            $model->getStatusMsg()
                        );

                    }

					$model->setData('git_revision',$model->_getGitHEADId());
					try{
						$model->save();

                        Mage::getSingleton('adminhtml/session')->addSuccess($model->getModmanName().Mage::helper('modman')->__(' successfully updated'));
                    }
					catch (Exception $e) {
					    Mage::logException($e);
					}
    			}
    			Mage::getSingleton('adminhtml/session')->addSuccess(
    			Mage::helper('modman')->__(
    			'Total of %d repo(s) were updated.', count($resourceIds)
    			)
    			);
    		} catch (Exception $e) {
    			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
    		}
    	}
    	$this->_redirect('*/*/index');
    }
 
    
    public function massDeployAction()
    {
    	$resourceIds = $this->getRequest()->getParam('resource_id');      // $this->getMassactionBlock()->setFormFieldName('tax_id'); from Mage_Adminhtml_Block_Tax_Rate_Grid
    	if(!is_array($resourceIds)) {
    		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('modman')->__('Please select modul(es).'));
    	} else {
    		try {
    			$modmanModel = Mage::getModel('modman/modules');
    			foreach ($resourceIds as $modulId) {

    				$modmanModel->load($modulId)->deploy();
    			}
    			Mage::getSingleton('adminhtml/session')->addSuccess(
    			Mage::helper('modman')->__(
    			'Total of %d repo(s) were deployed.', count($resourceIds)
    			)
    			);
    		} catch (Exception $e) {
    			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
    		}
    	}
    	$this->_redirect('*/*/index');
    }
 
    
    public function massExportAction()
    {
    	$resourceIds = $this->getRequest()->getParam('resource_id');      // $this->getMassactionBlock()->setFormFieldName('tax_id'); from Mage_Adminhtml_Block_Tax_Rate_Grid
    	if(!is_array($resourceIds)) {
    		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('modman')->__('Please select modul(es).'));
    	} else {
    		try {
    			$modmanModel = Mage::getModel('modman/modules');
    			foreach ($resourceIds as $modulId) {

    				$modmanModel->load($modulId)->export();
    			}
    			Mage::getSingleton('adminhtml/session')->addSuccess(
    			Mage::helper('modman')->__(
    			'Total of %d repo(s) were exported.', count($resourceIds)
    			)
    			);
    		} catch (Exception $e) {
    			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
    		}
    	}
    	$this->_redirect('*/*/index');
    }

    public function massConvertAction()
    {
        $resourceIds = $this->getRequest()->getParam('resource_id');      // $this->getMassactionBlock()->setFormFieldName('tax_id'); from Mage_Adminhtml_Block_Tax_Rate_Grid
        if(!is_array($resourceIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('modman')->__('Please select modul(es).'));
        } else {
            try {
                $modmanModel = Mage::getModel('modman/modules');
                $i = 0;
                foreach ($resourceIds as $modulId) {

                    $modulData = $modmanModel->load($modulId);
                    if(Mage::getModel('modman/modman_convert')->_convert($modulData)){

                        $i++;
                    }

                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('modman')->__(
                        'Total of %s from %s repo(s) were converted.', $i, count($resourceIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function updateModmanAction(){


        chdir(Mage::getBaseDir('base'));
        exec(Mage::Helper('modman')->modmanCmd().'update highdigital-modman --force  2>&1', $result);
        $resultString = implode('<br/>',$result);

        if(strpos($resultString,'has already been checked out') || strpos($resultString,'Error trying to update')){
            Mage::getSingleton('adminhtml/session')->addError($resultString);

        } else {
            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('modman')->__('Highdigital Modman wurde erfolgreich aktualisiert.')
            );

        }
        $this->_redirect('*/*/index');
    }
}