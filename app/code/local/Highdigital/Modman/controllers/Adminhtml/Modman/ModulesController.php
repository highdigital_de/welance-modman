<?php
class Highdigital_Modman_Adminhtml_Modman_ModulesController extends Mage_Adminhtml_Controller_Action{
	public function preDispatch(){
		
		return parent::preDispatch();
	}
	
	public function indexAction(){
		$this->loadLayout();
		$this->renderLayout();
		
	}


    public function newAction()
    {
//        $id = $this->getRequest()->getParam('resource_id', null);
//        $model = Mage::getModel('modman/modules');
//        Mage::register('modman_data', $model);

        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->renderLayout();
    }

	protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('modman');
    }
	public function modmaninitAction(){
		
		$storeId = $this->getRequest()->getParam('store');
		try{

			exec(Mage::getBaseDir('base').'/modman init' , $result, $return);
			$result = implode('<br/>',$result);
			$this->_getSession()->addSuccess($result);
			
		}
		catch(Exception $e){
            Mage::logException($e);
		}
		
        $this->_redirect('*/*/', array('store'=>$storeId));

	}

    public function initProjectAction(){
        $storeId = $this->getRequest()->getParam('store');

        try{
            Mage::getModel('modman/modules')->_initProject();
        }
        catch (Exception $e){

            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            $redirectBack = true;

        }
        $this->_redirect('*/modman', array('store'=>$storeId));

    }

    public function savekeysAction(){

        $keydata = $this->getRequest()->getParams();
        try{
            Mage::getModel('modman/modules')->_saveKeys($keydata);
        }
        catch (Exception $e){

            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            $redirectBack = true;

        }
        $this->_redirect('*/modman_modules');

    }

    public function addkeyAction(){
        try{
            Mage::getModel('modman/modules')->setGithubPublicKey();
        }
        catch (Exception $e){

            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            $redirectBack = true;

        }
        $this->_redirect('*/modman_modules');

    }

    public function convertRepoAction(){
        $storeId = $this->getRequest()->getParam('store');

        try{
            Mage::getModel('modman/modman_convert')->_convertRepos();
        }
        catch (Exception $e){

            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            $redirectBack = true;

        }
        $this->_redirect('*/modman', array('store'=>$storeId));

    }

    public function setsshconfigAction(){
        $storeId = $this->getRequest()->getParam('store');

        try{
            Mage::getModel('modman/modules')->_initConfig();
            $this->_getSession()->addSuccess('Config file was successfully created');
        }
        catch (Exception $e){

            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
            $redirectBack = true;

        }
        $this->_redirect('*/*/', array('store'=>$storeId));

    }
	
	public function initAction(){
		$storeId = $this->getRequest()->getParam('store');
		try{
            	Mage::getModel('modman/modules')->_initPublicKey();
                Mage::getModel('modman/modules')->_initConfig();
//                Mage::getModel('modman/modules')->_initKnownHosts();
        		$this->_redirect('*/*/', array('store'=>$storeId));
		}
		catch (Exception $e){
			
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
                $redirectBack = true;
			
		}
        $this->_redirect('*/*/', array('store'=>$storeId));
	}
	public function spkAction(){
		
        $this->_redirect('*/*/', array('showPK'=>true));
	}
}