<?php

class Highdigital_Modman_Helper_Data extends Mage_Core_Helper_Data{

    const MODMAN_IS_GLOBAL = 'modman/config/is_global';

    public function modmanCmd(){

        return Mage::getStoreConfig(self::MODMAN_IS_GLOBAL)?'modman ':Mage::getBaseDir('base').'/modman ';
    }
}