<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category  Highdigital
 * @package   Welance_Modman
 * @author    highdigital GbR <hello@highdigital.de>
 * @author    Niels Gongoll <ng@highdigital.de>
 * @copyright 2012 highdigital GbR
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      http://www.welance.de/
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();
/**
 * export file table
 */
$tableName = $installer->getTable('modman/modules');

$table = $connection->newTable($tableName)
		->addColumn('resource_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'identity' => true,
            'nullable' => false,
            'primary' => true,
                ), 'Resource id'
        )
        ->addColumn('git_clone_url', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'identity' => false,
            'nullable' => false,
            'primary' => false,
                ), 'URL git clone'
        )
        ->addColumn('sort_order', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'identity' => false,	
            'nullable' => false,
            'primary' => false,
                ), 'Sort order'
        )
        ->addColumn('modman_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'identity' => false,
            'nullable' => false,
            'primary' => false,
                ), 'Resource name'
        )
        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'identity' => false,
            'nullable' => false,
            'primary' => false,
                ), 'Status'
        )
        ->setComment('Modman modul table');
$connection->createTable($table);
/**
 * export file table end
 */
$installer->endSetup();