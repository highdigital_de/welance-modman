<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category  Highdigital
 * @package   Welance_Modman
 * @author    highdigital GbR <hello@highdigital.de>
 * @author    Niels Gongoll <ng@highdigital.de>
 * @copyright 2012 highdigital GbR
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      http://www.welance.de/
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
 
$connection = $installer->getConnection();
$tableName = $installer->getTable('modman/modules');
$connection->query("ALTER TABLE `{$tableName}` ADD COLUMN commit_date VARCHAR(12) NOT NULL");
$connection->query("ALTER TABLE `{$tableName}` ADD COLUMN last_update VARCHAR(12) NOT NULL");
   

$installer->endSetup();